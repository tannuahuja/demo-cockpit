from flask import Flask, request, render_template
import requests  

app = Flask(__name__)

gitlab_url = "https://gitlab.com"
project_id = "51066584"  
trigger_token = "glptt-33a3eeccda6b6ca95551c337cb40f07131292449" 
branch = "main" 

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        name = request.form.get('name')
        if name:
            greeting = f'Hello, {name}!'
            
            api_endpoint = f"{gitlab_url}/api/v4/projects/{project_id}/trigger/pipeline"

            data = {"token": trigger_token, "ref": branch}

            response = requests.post(api_endpoint, data=data)

            if response.status_code == 200 or response.status_code == 201:
                pipeline_status = "Pipeline triggered successfully"
            else:
                pipeline_status = f"Failed to trigger pipeline - Response code: {response.status_code}"

            return render_template('index.html', greeting=greeting, pipeline_status=pipeline_status)

    return render_template('index.html', greeting='', pipeline_status='')

if __name__ == '__main__':
    app.run(debug=True)
